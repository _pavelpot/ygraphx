# Math
# Math extension

# constants
Math.PI_DIV_180 = Math.PI / 180

Math.degtorad = (deg) -> 
# Converts degrees to radians

Math.rotate = (x,y,angle_deg) ->
# Rotation around the coordinate axis

# Function
# Function extension

Function::use = (argv...) ->
# Extends the prototype properties of prototypes of functions that are passed as arguments

Function::attr_reader = (argv...) ->
# Defines getter methods

Function::attr_writer = (argv...) ->
# Defines settor methods

Function::attr_accessor = (argv...) ->
# Defines accessor methods

class Options
# Trait for more user-friendly using of accessors

  set_options: (options) ->
  # Allows you to use multiple setters

class DisplayObject
# Base class for all display objects

