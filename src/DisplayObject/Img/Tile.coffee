class DisplayObject.Img.Tile extends DisplayObject.Img

  @attr_reader 'tile_width', 'tile_height'

  @attr_accessor [
    'tile_x', 'tile_y'
  ]...
  
  tile_width: 0
  
  tile_height: 0
  
  tile_x: 0
  
  tile_y: 0
  
  set_image: (img) ->
    @image = img
    @
  
  set_tile_width: (width) ->
    @tile_width = width
    @width = width*@scale_x
    @

  set_tile_height: (height) ->
    @tile_height = height
    @height = height*@scale_y
    @

  render: (parent_state) ->
    state = @inherit_state parent_state
    
    if state.renderer && state.visible
      i = @get_image()
      tile_width = @get_tile_width()
      tile_height = @get_tile_height()
      state.renderer.draw_image i, 
        tile_width*@get_tile_x(), 
        tile_height*@get_tile_y(), 
        tile_width,
        tile_height,
        state.real_x, 
        state.real_y, 
        state.width, 
        state.height, 
        state.alpha, 
        state.angle
    @