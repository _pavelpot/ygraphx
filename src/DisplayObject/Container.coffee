class DisplayObject.Container extends DisplayObject

  childs: null
  
  @attr_reader 'childs'
  
  constructor: (options) ->
    @childs = []
    super(options)

  add_child: (display_object...) ->
    for obj in display_object
      obj.set_parent @
    @childs.push display_object...
    @
    
  call_child: (method_name, data=[]) ->
    for child in @childs
      child[method_name] data...
    @

  render: (parent_state) ->
    state = @inherit_state parent_state
    @call_child 'render', [state]
    # for child in @childs
      # child.dispatch_event 'render', state
      # child.render state
    @
  
  enter_frame: () ->
    @call_child 'enter_frame'