class DisplayObject.Img extends DisplayObject

  @attr_reader 'image'

  image: null

  set_image: (img) ->
    @image = img
    @width = img.width
    @height = img.height
    @

  render: (parent_state) ->
    state = @inherit_state parent_state
    
    if state.renderer && state.visible
      i = @get_image()
      state.renderer.draw_image i, 
        0, 
        0, 
        i.width, 
        i.height, 
        state.real_x, 
        state.real_y, 
        state.width, 
        state.height, 
        state.alpha, 
        state.angle
    @