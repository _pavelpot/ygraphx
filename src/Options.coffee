# % comment_mode
class Options
# Trait for more user-friendly using of accessors

  set_options: (options) ->
  # Allows you to use multiple setters
  # % comment_mode
    for key, value of options
      @["set_#{key}"](value)
      # @["set_#{key}"]?(value)
    @