# % comment_mode
# Function
# Function extension

Function::use = (argv...) ->
# Extends the prototype properties of prototypes of functions that are passed as arguments
# % comment_mode
  for cls in argv
    for key, value of cls::
      @::[key]=value
  @

# % comment_mode
Function::attr_reader = (argv...) ->
# Defines getter methods
# % comment_mode
  for attr in argv
    do (attr) =>
      @::["get_#{attr}"] = -> @[attr]
  @

# % comment_mode
Function::attr_writer = (argv...) ->
# Defines settor methods
# % comment_mode
  for attr in argv
    do (attr) =>
      @::["set_#{attr}"] = (value) -> @[attr]=value
  @

# % comment_mode
Function::attr_accessor = (argv...) ->
# Defines accessor methods
# % comment_mode
  @attr_reader argv...
  @attr_writer argv...
  @