# % require : Math, Function, Options, EventDispatcher
# % comment_mode
class DisplayObject
# Base class for all display objects
# % comment_mode
  id = 0

  @use Options # , EventDispatcher

  @attr_accessor [
    'id'
    'parent', 'renderer'
    'visible', 'alpha'
    'x','y'
    'angle'
    'pivot_x', 'pivot_y'
  ]...

  @attr_reader [
    'width', 'height'
    'scale_x', 'scale_y'
  ]...

  id: null

  parent: null

  renderer: null

  visible: true

  alpha: 1

  x: 0

  y: 0

  angle: 0

  pivot_x: 0

  pivot_y: 0

  width: 0

  height: 0

  scale_x: 1

  scale_y: 1

  constructor: (options) ->
    @set_options options if options?
    @set_id id++
    # @add_event_listener 'render', (e) => @render(e)

  enter_frame: ->
    @
    
  render: (parent_state) ->
    @

  set_width: (w) ->
    @scale_x = (w/@width)*@scale_x
    @width=w
    @

  set_height: (h) ->
    @scale_y = (h/@height)*@scale_y
    @height=h
    @

  set_scale_x: (sx) ->
    @width = @width * (sx/@scale_x)
    @scale_x=sx
    @

  set_scale_y: (sy) ->
    @height = @height * (sy/@scale_y)
    @scale_y=sy
    @

  common_attr: (attr) ->
    value = [@["get_#{attr}"]?()]
    parent = @get_parent()
    value = (parent.common_attr attr).concat value if parent?
    value

  inherit_state: (state = null) ->
    visible = @get_visible() 
    alpha = @get_alpha()
    scale_x = @get_scale_x()
    scale_y = @get_scale_y()
    width = @get_width()
    height = @get_height()
    angle = @get_angle()
    pivot_x = @get_pivot_x()
    pivot_y = @get_pivot_y()
    
    renderer = @get_renderer() 
    
    x = @get_x()
    y = @get_y()
    
    real_pivot_x = x + pivot_x
    real_pivot_y = y + pivot_y
    
    if state != null
      
      renderer = state.renderer
      
      visible &= state.visible
      alpha *= state.alpha
      scale_x *= state.scale_x
      scale_y *= state.scale_y
      width *= state.scale_x
      height *= state.scale_y
      angle += state.angle
      
      [real_pivot_x_offset, real_pivot_y_offset] = Math.rotate -state.pivot_x+x+pivot_x, -state.pivot_y+y+pivot_y, state.angle
      
      x += state.x
      y += state.y
      
      real_pivot_x = state.x + state.pivot_x + real_pivot_x_offset
      real_pivot_y = state.y + state.pivot_y + real_pivot_y_offset
      
    [real_x_offset, real_y_offset] = Math.rotate -pivot_x, -pivot_y, angle
    
    real_x = real_pivot_x + real_x_offset
    real_y = real_pivot_y + real_y_offset
    
    {
    x,
    y,
    visible
    alpha
    scale_x
    scale_y
    width
    height
    angle
    pivot_x
    pivot_y
    real_x
    real_y
    renderer
    }
    