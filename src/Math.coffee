# % comment_mode
# Math
# Math extension

# constants
Math.PI_DIV_180 = Math.PI / 180

Math.degtorad = (deg) -> 
# Converts degrees to radians
# % comment_mode
  deg * Math.PI_DIV_180

# % comment_mode
Math.rotate = (x,y,angle_deg) ->
# Rotation around the coordinate axis
# % comment_mode
  angle_rad=Math.degtorad(angle_deg)
  [x*Math.cos(angle_rad) - y*Math.sin(angle_rad), x*Math.sin(angle_rad) + y*Math.cos(angle_rad)]

###
Math.round = (value) ->
    value+0.5|0
###