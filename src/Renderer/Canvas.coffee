class Renderer.Canvas extends Renderer

  canvas: null

  context: null

  @attr_reader 'canvas'

  @attr_accessor 'context'

  set_canvas: (c) ->
    @canvas = c
    @set_context c.getContext '2d'
    @

  draw_image: (image, sx, sy, sw, sh, dx, dy, dw, dh, alpha, angle) ->
    context = @get_context()
    context.save()
    context.translate dx, dy
    if angle!=0
      context.rotate Math.degtorad angle
    if alpha<1
      context.globalAlpha = alpha
    context.drawImage image, sx, sy, sw, sh, 0, 0, dw, dh
    context.restore()
    @

  clear: ->
    canvas = @get_canvas()
    @get_context().clearRect 0, 0, canvas.width, canvas.height
    @