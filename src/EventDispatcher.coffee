class EventDispatcher

  event_listeners: null

  add_event_listener: (type, listener) ->
    @event_listeners={} if !@event_listeners
    @event_listeners[type]=[] if !@event_listeners[type]
    @event_listeners[type].push listener
    @

  dispatch_event: (type, data) ->
    if @event_listeners[type]
      for listener in @event_listeners[type]
        listener?(data)
    @

  remove_event_listener: (type, listener) ->
    delete @event_listeners[type][index] if (index = @event_listeners[type].indexOf listener)>=0
    @