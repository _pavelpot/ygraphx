# % comment_mode
# Math
# Math extension

# constants
Math.PI_DIV_180 = Math.PI / 180

Math.degtorad = (deg) -> 
# Converts degrees to radians
# % comment_mode
  deg * Math.PI_DIV_180

# % comment_mode
Math.rotate = (x,y,angle_deg) ->
# Rotation around the coordinate axis
# % comment_mode
  angle_rad=Math.degtorad(angle_deg)
  [x*Math.cos(angle_rad) - y*Math.sin(angle_rad), x*Math.sin(angle_rad) + y*Math.cos(angle_rad)]

###
Math.round = (value) ->
    value+0.5|0
###

# % comment_mode
# Function
# Function extension

Function::use = (argv...) ->
# Extends the prototype properties of prototypes of functions that are passed as arguments
# % comment_mode
  for cls in argv
    for key, value of cls::
      @::[key]=value
  @

# % comment_mode
Function::attr_reader = (argv...) ->
# Defines getter methods
# % comment_mode
  for attr in argv
    do (attr) =>
      @::["get_#{attr}"] = -> @[attr]
  @

# % comment_mode
Function::attr_writer = (argv...) ->
# Defines settor methods
# % comment_mode
  for attr in argv
    do (attr) =>
      @::["set_#{attr}"] = (value) -> @[attr]=value
  @

# % comment_mode
Function::attr_accessor = (argv...) ->
# Defines accessor methods
# % comment_mode
  @attr_reader argv...
  @attr_writer argv...
  @

# % comment_mode
class Options
# Trait for more user-friendly using of accessors

  set_options: (options) ->
  # Allows you to use multiple setters
  # % comment_mode
    for key, value of options
      @["set_#{key}"](value)
      # @["set_#{key}"]?(value)
    @

class EventDispatcher

  event_listeners: null

  add_event_listener: (type, listener) ->
    @event_listeners={} if !@event_listeners
    @event_listeners[type]=[] if !@event_listeners[type]
    @event_listeners[type].push listener
    @

  dispatch_event: (type, data) ->
    if @event_listeners[type]
      for listener in @event_listeners[type]
        listener?(data)
    @

  remove_event_listener: (type, listener) ->
    delete @event_listeners[type][index] if (index = @event_listeners[type].indexOf listener)>=0
    @

# % require : Math, Function, Options, EventDispatcher
# % comment_mode
class DisplayObject
# Base class for all display objects
# % comment_mode
  id = 0

  @use Options # , EventDispatcher

  @attr_accessor [
    'id'
    'parent', 'renderer'
    'visible', 'alpha'
    'x','y'
    'angle'
    'pivot_x', 'pivot_y'
  ]...

  @attr_reader [
    'width', 'height'
    'scale_x', 'scale_y'
  ]...

  id: null

  parent: null

  renderer: null

  visible: true

  alpha: 1

  x: 0

  y: 0

  angle: 0

  pivot_x: 0

  pivot_y: 0

  width: 0

  height: 0

  scale_x: 1

  scale_y: 1

  constructor: (options) ->
    @set_options options if options?
    @set_id id++
    # @add_event_listener 'render', (e) => @render(e)

  enter_frame: ->
    @
    
  render: (parent_state) ->
    @

  set_width: (w) ->
    @scale_x = (w/@width)*@scale_x
    @width=w
    @

  set_height: (h) ->
    @scale_y = (h/@height)*@scale_y
    @height=h
    @

  set_scale_x: (sx) ->
    @width = @width * (sx/@scale_x)
    @scale_x=sx
    @

  set_scale_y: (sy) ->
    @height = @height * (sy/@scale_y)
    @scale_y=sy
    @

  common_attr: (attr) ->
    value = [@["get_#{attr}"]?()]
    parent = @get_parent()
    value = (parent.common_attr attr).concat value if parent?
    value

  inherit_state: (state = null) ->
    visible = @get_visible() 
    alpha = @get_alpha()
    scale_x = @get_scale_x()
    scale_y = @get_scale_y()
    width = @get_width()
    height = @get_height()
    angle = @get_angle()
    pivot_x = @get_pivot_x()
    pivot_y = @get_pivot_y()
    
    renderer = @get_renderer() 
    
    x = @get_x()
    y = @get_y()
    
    real_pivot_x = x + pivot_x
    real_pivot_y = y + pivot_y
    
    if state != null
      
      renderer = state.renderer
      
      visible &= state.visible
      alpha *= state.alpha
      scale_x *= state.scale_x
      scale_y *= state.scale_y
      width *= state.scale_x
      height *= state.scale_y
      angle += state.angle
      
      [real_pivot_x_offset, real_pivot_y_offset] = Math.rotate -state.pivot_x+x+pivot_x, -state.pivot_y+y+pivot_y, state.angle
      
      x += state.x
      y += state.y
      
      real_pivot_x = state.x + state.pivot_x + real_pivot_x_offset
      real_pivot_y = state.y + state.pivot_y + real_pivot_y_offset
      
    [real_x_offset, real_y_offset] = Math.rotate -pivot_x, -pivot_y, angle
    
    real_x = real_pivot_x + real_x_offset
    real_y = real_pivot_y + real_y_offset
    
    {
    x,
    y,
    visible
    alpha
    scale_x
    scale_y
    width
    height
    angle
    pivot_x
    pivot_y
    real_x
    real_y
    renderer
    }
    

# % require : Function, Options
class Renderer

  @use Options

  constructor: (options) ->
    @set_options options if options?

class DisplayObject.Container extends DisplayObject

  childs: null
  
  @attr_reader 'childs'
  
  constructor: (options) ->
    @childs = []
    super(options)

  add_child: (display_object...) ->
    for obj in display_object
      obj.set_parent @
    @childs.push display_object...
    @
    
  call_child: (method_name, data=[]) ->
    for child in @childs
      child[method_name] data...
    @

  render: (parent_state) ->
    state = @inherit_state parent_state
    @call_child 'render', [state]
    # for child in @childs
      # child.dispatch_event 'render', state
      # child.render state
    @
  
  enter_frame: () ->
    @call_child 'enter_frame'

class DisplayObject.Img extends DisplayObject

  @attr_reader 'image'

  image: null

  set_image: (img) ->
    @image = img
    @width = img.width
    @height = img.height
    @

  render: (parent_state) ->
    state = @inherit_state parent_state
    
    if state.renderer && state.visible
      i = @get_image()
      state.renderer.draw_image i, 
        0, 
        0, 
        i.width, 
        i.height, 
        state.real_x, 
        state.real_y, 
        state.width, 
        state.height, 
        state.alpha, 
        state.angle
    @

class DisplayObject.Img.Tile extends DisplayObject.Img

  @attr_reader 'tile_width', 'tile_height'

  @attr_accessor [
    'tile_x', 'tile_y'
  ]...
  
  tile_width: 0
  
  tile_height: 0
  
  tile_x: 0
  
  tile_y: 0
  
  set_image: (img) ->
    @image = img
    @
  
  set_tile_width: (width) ->
    @tile_width = width
    @width = width*@scale_x
    @

  set_tile_height: (height) ->
    @tile_height = height
    @height = height*@scale_y
    @

  render: (parent_state) ->
    state = @inherit_state parent_state
    
    if state.renderer && state.visible
      i = @get_image()
      tile_width = @get_tile_width()
      tile_height = @get_tile_height()
      state.renderer.draw_image i, 
        tile_width*@get_tile_x(), 
        tile_height*@get_tile_y(), 
        tile_width,
        tile_height,
        state.real_x, 
        state.real_y, 
        state.width, 
        state.height, 
        state.alpha, 
        state.angle
    @

class Renderer.Canvas extends Renderer

  canvas: null

  context: null

  @attr_reader 'canvas'

  @attr_accessor 'context'

  set_canvas: (c) ->
    @canvas = c
    @set_context c.getContext '2d'
    @

  draw_image: (image, sx, sy, sw, sh, dx, dy, dw, dh, alpha, angle) ->
    context = @get_context()
    context.save()
    context.translate dx, dy
    if angle!=0
      context.rotate Math.degtorad angle
    if alpha<1
      context.globalAlpha = alpha
    context.drawImage image, sx, sy, sw, sh, 0, 0, dw, dh
    context.restore()
    @

  clear: ->
    canvas = @get_canvas()
    @get_context().clearRect 0, 0, canvas.width, canvas.height
    @

