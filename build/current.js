var DisplayObject, EventDispatcher, Options, Renderer,
  __slice = Array.prototype.slice,
  __hasProp = Object.prototype.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

Math.PI_DIV_180 = Math.PI / 180;

Math.degtorad = function(deg) {
  return deg * Math.PI_DIV_180;
};

Math.rotate = function(x, y, angle_deg) {
  var angle_rad;
  angle_rad = Math.degtorad(angle_deg);
  return [x * Math.cos(angle_rad) - y * Math.sin(angle_rad), x * Math.sin(angle_rad) + y * Math.cos(angle_rad)];
};

/*
Math.round = (value) ->
    value+0.5|0
*/

Function.prototype.use = function() {
  var argv, cls, key, value, _i, _len, _ref;
  argv = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  for (_i = 0, _len = argv.length; _i < _len; _i++) {
    cls = argv[_i];
    _ref = cls.prototype;
    for (key in _ref) {
      value = _ref[key];
      this.prototype[key] = value;
    }
  }
  return this;
};

Function.prototype.attr_reader = function() {
  var argv, attr, _fn, _i, _len,
    _this = this;
  argv = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  _fn = function(attr) {
    return _this.prototype["get_" + attr] = function() {
      return this[attr];
    };
  };
  for (_i = 0, _len = argv.length; _i < _len; _i++) {
    attr = argv[_i];
    _fn(attr);
  }
  return this;
};

Function.prototype.attr_writer = function() {
  var argv, attr, _fn, _i, _len,
    _this = this;
  argv = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  _fn = function(attr) {
    return _this.prototype["set_" + attr] = function(value) {
      return this[attr] = value;
    };
  };
  for (_i = 0, _len = argv.length; _i < _len; _i++) {
    attr = argv[_i];
    _fn(attr);
  }
  return this;
};

Function.prototype.attr_accessor = function() {
  var argv;
  argv = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
  this.attr_reader.apply(this, argv);
  this.attr_writer.apply(this, argv);
  return this;
};

Options = (function() {

  function Options() {}

  Options.prototype.set_options = function(options) {
    var key, value;
    for (key in options) {
      value = options[key];
      this["set_" + key](value);
    }
    return this;
  };

  return Options;

})();

EventDispatcher = (function() {

  function EventDispatcher() {}

  EventDispatcher.prototype.event_listeners = null;

  EventDispatcher.prototype.add_event_listener = function(type, listener) {
    if (!this.event_listeners) this.event_listeners = {};
    if (!this.event_listeners[type]) this.event_listeners[type] = [];
    this.event_listeners[type].push(listener);
    return this;
  };

  EventDispatcher.prototype.dispatch_event = function(type, data) {
    var listener, _i, _len, _ref;
    if (this.event_listeners[type]) {
      _ref = this.event_listeners[type];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        listener = _ref[_i];
        if (typeof listener === "function") listener(data);
      }
    }
    return this;
  };

  EventDispatcher.prototype.remove_event_listener = function(type, listener) {
    var index;
    if ((index = this.event_listeners[type].indexOf(listener)) >= 0) {
      delete this.event_listeners[type][index];
    }
    return this;
  };

  return EventDispatcher;

})();

DisplayObject = (function() {
  var id;

  id = 0;

  DisplayObject.use(Options);

  DisplayObject.attr_accessor.apply(DisplayObject, ['id', 'parent', 'renderer', 'visible', 'alpha', 'x', 'y', 'angle', 'pivot_x', 'pivot_y']);

  DisplayObject.attr_reader.apply(DisplayObject, ['width', 'height', 'scale_x', 'scale_y']);

  DisplayObject.prototype.id = null;

  DisplayObject.prototype.parent = null;

  DisplayObject.prototype.renderer = null;

  DisplayObject.prototype.visible = true;

  DisplayObject.prototype.alpha = 1;

  DisplayObject.prototype.x = 0;

  DisplayObject.prototype.y = 0;

  DisplayObject.prototype.angle = 0;

  DisplayObject.prototype.pivot_x = 0;

  DisplayObject.prototype.pivot_y = 0;

  DisplayObject.prototype.width = 0;

  DisplayObject.prototype.height = 0;

  DisplayObject.prototype.scale_x = 1;

  DisplayObject.prototype.scale_y = 1;

  function DisplayObject(options) {
    if (options != null) this.set_options(options);
    this.set_id(id++);
  }

  DisplayObject.prototype.enter_frame = function() {
    return this;
  };

  DisplayObject.prototype.render = function(parent_state) {
    return this;
  };

  DisplayObject.prototype.set_width = function(w) {
    this.scale_x = (w / this.width) * this.scale_x;
    this.width = w;
    return this;
  };

  DisplayObject.prototype.set_height = function(h) {
    this.scale_y = (h / this.height) * this.scale_y;
    this.height = h;
    return this;
  };

  DisplayObject.prototype.set_scale_x = function(sx) {
    this.width = this.width * (sx / this.scale_x);
    this.scale_x = sx;
    return this;
  };

  DisplayObject.prototype.set_scale_y = function(sy) {
    this.height = this.height * (sy / this.scale_y);
    this.scale_y = sy;
    return this;
  };

  DisplayObject.prototype.common_attr = function(attr) {
    var parent, value, _name;
    value = [typeof this[_name = "get_" + attr] === "function" ? this[_name]() : void 0];
    parent = this.get_parent();
    if (parent != null) value = (parent.common_attr(attr)).concat(value);
    return value;
  };

  DisplayObject.prototype.inherit_state = function(state) {
    var alpha, angle, height, pivot_x, pivot_y, real_pivot_x, real_pivot_x_offset, real_pivot_y, real_pivot_y_offset, real_x, real_x_offset, real_y, real_y_offset, renderer, scale_x, scale_y, visible, width, x, y, _ref, _ref2;
    if (state == null) state = null;
    visible = this.get_visible();
    alpha = this.get_alpha();
    scale_x = this.get_scale_x();
    scale_y = this.get_scale_y();
    width = this.get_width();
    height = this.get_height();
    angle = this.get_angle();
    pivot_x = this.get_pivot_x();
    pivot_y = this.get_pivot_y();
    renderer = this.get_renderer();
    x = this.get_x();
    y = this.get_y();
    real_pivot_x = x + pivot_x;
    real_pivot_y = y + pivot_y;
    if (state !== null) {
      renderer = state.renderer;
      visible &= state.visible;
      alpha *= state.alpha;
      scale_x *= state.scale_x;
      scale_y *= state.scale_y;
      width *= state.scale_x;
      height *= state.scale_y;
      angle += state.angle;
      _ref = Math.rotate(-state.pivot_x + x + pivot_x, -state.pivot_y + y + pivot_y, state.angle), real_pivot_x_offset = _ref[0], real_pivot_y_offset = _ref[1];
      x += state.x;
      y += state.y;
      real_pivot_x = state.x + state.pivot_x + real_pivot_x_offset;
      real_pivot_y = state.y + state.pivot_y + real_pivot_y_offset;
    }
    _ref2 = Math.rotate(-pivot_x, -pivot_y, angle), real_x_offset = _ref2[0], real_y_offset = _ref2[1];
    real_x = real_pivot_x + real_x_offset;
    real_y = real_pivot_y + real_y_offset;
    return {
      x: x,
      y: y,
      visible: visible,
      alpha: alpha,
      scale_x: scale_x,
      scale_y: scale_y,
      width: width,
      height: height,
      angle: angle,
      pivot_x: pivot_x,
      pivot_y: pivot_y,
      real_x: real_x,
      real_y: real_y,
      renderer: renderer
    };
  };

  return DisplayObject;

})();

Renderer = (function() {

  Renderer.use(Options);

  function Renderer(options) {
    if (options != null) this.set_options(options);
  }

  return Renderer;

})();

DisplayObject.Container = (function(_super) {

  __extends(Container, _super);

  Container.prototype.childs = null;

  Container.attr_reader('childs');

  function Container(options) {
    this.childs = [];
    Container.__super__.constructor.call(this, options);
  }

  Container.prototype.add_child = function() {
    var display_object, obj, _i, _len, _ref;
    display_object = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    for (_i = 0, _len = display_object.length; _i < _len; _i++) {
      obj = display_object[_i];
      obj.set_parent(this);
    }
    (_ref = this.childs).push.apply(_ref, display_object);
    return this;
  };

  Container.prototype.call_child = function(method_name, data) {
    var child, _i, _len, _ref;
    if (data == null) data = [];
    _ref = this.childs;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      child = _ref[_i];
      child[method_name].apply(child, data);
    }
    return this;
  };

  Container.prototype.render = function(parent_state) {
    var state;
    state = this.inherit_state(parent_state);
    this.call_child('render', [state]);
    return this;
  };

  Container.prototype.enter_frame = function() {
    return this.call_child('enter_frame');
  };

  return Container;

})(DisplayObject);

DisplayObject.Img = (function(_super) {

  __extends(Img, _super);

  function Img() {
    Img.__super__.constructor.apply(this, arguments);
  }

  Img.attr_reader('image');

  Img.prototype.image = null;

  Img.prototype.set_image = function(img) {
    this.image = img;
    this.width = img.width;
    this.height = img.height;
    return this;
  };

  Img.prototype.render = function(parent_state) {
    var i, state;
    state = this.inherit_state(parent_state);
    if (state.renderer && state.visible) {
      i = this.get_image();
      state.renderer.draw_image(i, 0, 0, i.width, i.height, state.real_x, state.real_y, state.width, state.height, state.alpha, state.angle);
    }
    return this;
  };

  return Img;

})(DisplayObject);

DisplayObject.Img.Tile = (function(_super) {

  __extends(Tile, _super);

  function Tile() {
    Tile.__super__.constructor.apply(this, arguments);
  }

  Tile.attr_reader('tile_width', 'tile_height');

  Tile.attr_accessor.apply(Tile, ['tile_x', 'tile_y']);

  Tile.prototype.tile_width = 0;

  Tile.prototype.tile_height = 0;

  Tile.prototype.tile_x = 0;

  Tile.prototype.tile_y = 0;

  Tile.prototype.set_image = function(img) {
    this.image = img;
    return this;
  };

  Tile.prototype.set_tile_width = function(width) {
    this.tile_width = width;
    this.width = width * this.scale_x;
    return this;
  };

  Tile.prototype.set_tile_height = function(height) {
    this.tile_height = height;
    this.height = height * this.scale_y;
    return this;
  };

  Tile.prototype.render = function(parent_state) {
    var i, state, tile_height, tile_width;
    state = this.inherit_state(parent_state);
    if (state.renderer && state.visible) {
      i = this.get_image();
      tile_width = this.get_tile_width();
      tile_height = this.get_tile_height();
      state.renderer.draw_image(i, tile_width * this.get_tile_x(), tile_height * this.get_tile_y(), tile_width, tile_height, state.real_x, state.real_y, state.width, state.height, state.alpha, state.angle);
    }
    return this;
  };

  return Tile;

})(DisplayObject.Img);

Renderer.Canvas = (function(_super) {

  __extends(Canvas, _super);

  function Canvas() {
    Canvas.__super__.constructor.apply(this, arguments);
  }

  Canvas.prototype.canvas = null;

  Canvas.prototype.context = null;

  Canvas.attr_reader('canvas');

  Canvas.attr_accessor('context');

  Canvas.prototype.set_canvas = function(c) {
    this.canvas = c;
    this.set_context(c.getContext('2d'));
    return this;
  };

  Canvas.prototype.draw_image = function(image, sx, sy, sw, sh, dx, dy, dw, dh, alpha, angle) {
    var context;
    context = this.get_context();
    context.save();
    context.translate(dx, dy);
    if (angle !== 0) context.rotate(Math.degtorad(angle));
    if (alpha < 1) context.globalAlpha = alpha;
    context.drawImage(image, sx, sy, sw, sh, 0, 0, dw, dh);
    context.restore();
    return this;
  };

  Canvas.prototype.clear = function() {
    var canvas;
    canvas = this.get_canvas();
    this.get_context().clearRect(0, 0, canvas.width, canvas.height);
    return this;
  };

  return Canvas;

})(Renderer);
