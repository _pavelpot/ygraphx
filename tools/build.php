<?php
class Coffee {
    
    protected $_files = array();

    protected $_output = '';
    
    protected $_comment = '';
    
    protected $_comment_mode = false;
    
    protected $_comment_str_id = 0;

    public function parse_dir($path) {
        $path = realpath($path);
        if (!file_exists($path)) {
            //dir not exists
            return;
        }
        if (!is_dir($path)) {
            //not dir
            return;
        }
        $contents = scandir($path);
        array_shift($contents);//.
        array_shift($contents);//..
        if (empty($contents)) {
            //empty dir
            return;
        }
        $cwdir = getcwd();
        chdir($path);
        $directories = array();
        foreach($contents as $el) {
            if (is_dir($el)) {
                array_push($directories, realpath($el));
            } else {
                $tmp = explode('.', $el);
                if (!empty($tmp)) {
                    if (array_pop($tmp)=='coffee') {
                        $this->parse_file($el);
                    }
                }
                unset($tmp);
            }
        }
        chdir($cwdir);
        if (!empty($directories)) {
            foreach ($directories as $dir) {
                $this->parse_dir($dir);
            }
        }
    }

    public function parse_file($path) {
        $path = realpath($path);
        if (!file_exists($path)) {
            //file not exists
            return;
        }
        if (!is_file($path)) {
            //not file
            return;
        }
        if (in_array($path, $this->_files)) {
            //file already loaded
            return;
        }
        array_push($this->_files, $path);
        $fh = fopen($path, 'r');
        while ($str = fgets($fh)) {
            preg_match('/.*?#\s%\s(.+?)[\r\n\f]+/', $str, $m);
            if (!empty($m)&&!empty($m[1])) {
                $commands = explode(' % ' ,$m[1]);
                foreach ($commands as $command) {
                    list($action, $options) = explode(' : ', $command);
                    if (!empty($options)) {
                        $options = explode(', ',$options);
                    } else {
                        $options = array();
                    }
                    $method = '_parse_file_fn_' . $action;
                    if (method_exists($this, $method)) {
                        call_user_func(array($this, $method), $options);
                    } else {
                        //method not exists
                    }
                }
            }
            $this->_parse_str($str);
        }
        $this->_output .= "\r\n\r\n";
        fclose($fh);
    }

    protected function _parse_str($str) {
        $this->_output .= $str;
        if ($this->_comment_mode) {
            if ($this->_comment_str_id>0) {
                $this->_comment.=$str;
            }
            $this->_comment_str_id++;
        }
    }

    protected function _parse_file_fn_require($files) {
        foreach($files as $file) {
            $this->parse_file($file . '.coffee');
        }
    }
    
    protected function _parse_file_fn_comment($lines) {
        foreach($lines as $line) {
            $this->_comment.=$line."\r\n";
        }
    }
    
    protected function _parse_file_fn_comment_mode_on() {
        $this->_comment_mode = true;
        $this->_comment_str_id = 0;
    }
    
    protected function _parse_file_fn_comment_mode_off() {
        $this->_comment_mode = false;
        $this->_comment.="\r\n";
    }
    
    protected function _parse_file_fn_comment_mode() {
        if ($this->_comment_mode) {
            $this->_parse_file_fn_comment_mode_off();
        } else {
            $this->_parse_file_fn_comment_mode_on();
        }
    }

    public function get_output() {
        return $this->_output;
    }
    
    public function get_comment() {
        return $this->_comment;
    }
}

$c = new Coffee();
$c->parse_dir('../src');
$output = $c->get_output();
$dc = $c->get_comment();
file_put_contents('../build/current.coffee', $output);
file_put_contents('../readme.txt', $dc);
?>
